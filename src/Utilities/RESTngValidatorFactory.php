<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 2018-12-04
 * Time: 13:56
 */
namespace MiamiOH\PositionWebService\Utilities;

use Illuminate\Validation\Validator;
use Illuminate\Translation;
use Illuminate\Filesystem\Filesystem;

class RESTngValidatorFactory
{
    public static function make(array $data, array $rules, array $messages = [], array $customAttributes = []) : Validator
    {
        return new Validator(self::loadTranslator(), $data, $rules, $messages, $customAttributes);
    }

    private static function loadTranslator()
    {
        $langPath = dirname(dirname(__FILE__)) . '/Utilities/lang';

        $filesystem = new Filesystem();

        $loader = new Translation\FileLoader(
            $filesystem,
            $langPath
        );

        $loader->addNamespace(
            'lang',
            $langPath
        );

        $loader->load('en', 'validation', 'lang');

        return new Translation\Translator($loader, 'en');
    }
}
