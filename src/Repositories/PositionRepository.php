<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 12/3/18
 * Time: 7:44 PM
 */

namespace MiamiOH\PositionWebService\Repositories;


use MiamiOH\PositionWebService\Services\Position;

interface PositionRepository
{
    public function createPosition(Position $positionObject);

    public function updatePosition(Position $positionObject);

    public function getPosition(string $positionNumber);
}