<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 12/3/18
 * Time: 7:41 PM
 */

namespace MiamiOH\PositionWebService\Repositories;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MiamiOH\PositionWebService\EloquentModels\EmployeeClass;
use MiamiOH\PositionWebService\EloquentModels\EmployeePosition;
use MiamiOH\PositionWebService\EloquentModels\EmploymentCategoryCode;
use MiamiOH\PositionWebService\EloquentModels\PositionBudgetTotals;
use MiamiOH\PositionWebService\EloquentModels\PositionClassification;
use MiamiOH\PositionWebService\EloquentModels\PositionGroup;
use MiamiOH\PositionWebService\EloquentModels\StandardOccupationalCategory;
use MiamiOH\PositionWebService\Services\Position;

/**
 * Class PositionSQL
 * @package MiamiOH\PositionWebService\Repositories
 */
class PositionSQL implements PositionRepository
{

    /**
     * @var
     */
    private $employeePosition;


    /**
     * @param Position $position
     * @return mixed
     * @throws \Exception
     */
    public function createPosition(Position $position)
    {
        $employeePosition = new EmployeePosition();
        return $employeePosition::create([
                'nbbposn_posn' => $position->getPosition(),
                'nbbposn_status' => $position->getStatus(),
                'nbbposn_title' => $position->getTitle(),
                'nbbposn_ploc_code' => $position->getPositionLocationCode(),
                'nbbposn_begin_date' => $position->getBeginDate(),
                'nbbposn_end_date' => $position->getEndDate(),
                'nbbposn_type' => $position->getPositionType(),
                'nbbposn_pcls_code' => $position->getPositionClassCode(),
                'nbbposn_ecls_code' => $position->getEmployeeClassCode(),
                'nbbposn_posn_reports' => $position->getPositionReportsTo(),
                'nbbposn_auth_number' => $position->getPositionAuthorizedNumber(),
                'nbbposn_table' => $position->getSalaryTable(),
                'nbbposn_grade' => $position->getSalaryGrade(),
                'nbbposn_step' => $position->getSalaryStep(),
                'nbbposn_appt_pct' => $position->getPositionAppointmentPercent(),
                'nbbposn_cipc_code' => $position->getCollegeInstructionalProgramCode(),
                'nbbposn_roll_ind' => $position->getBudgetRollIndicator(),
                'nbbposn_coas_code' => $position->getPositionChartOfAccountsCode(),
                'nbbposn_activity_date' => $position->getActivityDate(),
                'nbbposn_sgrp_code' => $position->getSalaryGroup(),
                'nbbposn_pgrp_code' => $position->getPositionGroupCode(),
                'nbbposn_wksh_code' => $position->getWorkScheduleCode(),
                'nbbposn_premium_roll_ind' => $position->getPremiumEarningsRollIndicator(),
                'nbbposn_pfoc_code' => $position->getFederalOccupationalCode(),
                'nbbposn_pnoc_code' => $position->getNationalOccupationalCode(),
                'nbbposn_dott_code' => $position->getOccupationalTitleCode(),
                'nbbposn_change_date_time' => $position->getChangeDateTime(),
                'nbbposn_calif_type' => $position->getCaliforniaType(),
                'nbbposn_exempt_ind' => $position->getExemptionIndicator(),
                'nbbposn_jbln_code' => $position->getJobLocationCode(),
                'nbbposn_barg_code' => $position->getBargainingUnitCode(),
                'nbbposn_probation_units' => $position->getProbationUnits(),
                'nbbposn_accrue_seniority_ind' => $position->getAccrueSeniorityIndicator(),
                'nbbposn_jobp_code' => $position->getJobProgressionCode(),
                'nbbposn_bpro_code' => $position->getBudgetProfileCode(),
                'nbbposn_budget_type' => $position->getBudgetType(),
                'nbbposn_esoc_code' => $position->getStandardOccupationalCategoryCode(),
                'nbbposn_ecip_code' => $position->getEmploymentCategoryCode(),
                'nbbposn_comment' => $position->getComments(),
                'nbbposn_user_id' => $position->getUserId(),
                'nbbposn_data_origin' => $position->getDataOrigin()]
        );
    }

    /**
     * @param Position $position
     * @return mixed
     * @throws \Exception
     */
    public function updatePosition(Position $position)
    {
        try {
            $model = EmployeePosition::where('nbbposn_posn', $position->getPosition())->first();
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException('Position not found.');
        }

        $modifiedAttributes = $position->getModifiedAttributes();

        if (isset($modifiedAttributes['status'])) {
            $model->nbbposn_status = $position->getStatus();
        }

        if (isset($modifiedAttributes['positionLocationCode'])) {
            $model->nbbposn_ploc_code = $position->getPositionLocationCode();
        }

        if (isset($modifiedAttributes['title'])) {
            $model->nbbposn_title = $position->getTitle();
        }

        if (isset($modifiedAttributes['beginDate'])) {
            $model->nbbposn_begin_date = $position->getBeginDate();
        }

        if (isset($modifiedAttributes['endDate'])) {
            $model->nbbposn_end_date = $position->getEndDate();
        }

        if (isset($modifiedAttributes['type'])) {
            $model->nbbposn_type = $position->getPositionType();
        }

        if (isset($modifiedAttributes['positionClassCode'])) {
            $model->nbbposn_pcls_code = $position->getPositionClassCode();
        }

        if (isset($modifiedAttributes['employeeClassCode'])) {
            $model->nbbposn_ecls_code = $position->getEmployeeClassCode();
        }

        if (isset($modifiedAttributes['positionReportsTo'])) {
            $model->nbbposn_posn_reports = $position->getPositionReportsTo();
        }

        if (isset($modifiedAttributes['positionAuthorizedNumber'])) {
            $model->nbbposn_auth_number = $position->getPositionAuthorizedNumber();
        }
        if (isset($modifiedAttributes['salaryTable'])) {
            $model->nbbposn_table = $position->getSalaryTable();
        }

        if (isset($modifiedAttributes['salaryGrade'])) {
            $model->nbbposn_grade = $position->getSalaryGrade();
        }

        if (isset($modifiedAttributes['salaryStep'])) {
            $model->nbbposn_step = $position->getSalaryStep();
        }

        if (isset($modifiedAttributes['positionAppointmentPercent'])) {
            $model->nbbposn_appt_pct = $position->getPositionAppointmentPercent();
        }

        if (isset($modifiedAttributes['collegeInstructionalProgramCode'])) {
            $model->nbbposn_cipc_code = $position->getCollegeInstructionalProgramCode();
        }

        if (isset($modifiedAttributes['budgetRollIndicator'])) {
            $model->nbbposn_roll_ind = $position->getBudgetRollIndicator();
        }

        if (isset($modifiedAttributes['positionChartOfAccountsCode'])) {
            $model->nbbposn_coas_code = $position->getPositionChartOfAccountsCode();
        }

        if (isset($modifiedAttributes['salaryGroup'])) {
            $model->nbbposn_sgrp_code = $position->getSalaryGroup();
        }

        if (isset($modifiedAttributes['positionGroupCode'])) {
            $model->nbbposn_pgrp_code = $position->getPositionGroupCode();
        }

        if (isset($modifiedAttributes['workScheduleCode'])) {
            $model->nbbposn_wksh_code = $position->getWorkScheduleCode();
        }

        if (isset($modifiedAttributes['premiumEarningsRollIndicator'])) {
            $model->nbbposn_premium_roll_ind = $position->getPremiumEarningsRollIndicator();
        }

        if (isset($modifiedAttributes['federalOccupationalCode'])) {
            $model->nbbposn_pfoc_code = $position->getFederalOccupationalCode();
        }

        if (isset($modifiedAttributes['nationalOccupationalCode'])) {
            $model->nbbposn_pnoc_code = $position->getNationalOccupationalCode();
        }

        if (isset($modifiedAttributes['occupationalTitleCode'])) {
            $model->nbbposn_dott_code = $position->getOccupationalTitleCode();
        }

        if (isset($modifiedAttributes['changeDateTime'])) {
            $model->nbbposn_change_date_time = $position->getChangeDateTime();
        }

        if (isset($modifiedAttributes['californiaType'])) {
            $model->nbbposn_calif_type = $position->getCaliforniaType();
        }

        if (isset($modifiedAttributes['exemptionIndicator'])) {
            $model->nbbposn_exempt_ind = $position->getExemptionIndicator();
        }

        if (isset($modifiedAttributes['jobLocationCode'])) {
            $model->nbbposn_jbln_code = $position->getJobLocationCode();
        }

        if (isset($modifiedAttributes['bargainingUnitCode'])) {
            $model->nbbposn_barg_code = $position->getBargainingUnitCode();
        }

        if (isset($modifiedAttributes['probationUnits'])) {
            $model->nbbposn_probation_units = $position->getProbationUnits();
        }

        if (isset($modifiedAttributes['accrueSeniorityIndicator'])) {
            $model->nbbposn_accrue_seniority_ind = $position->getAccrueSeniorityIndicator();
        }

        if (isset($modifiedAttributes['jobProgressionCode'])) {
            $model->nbbposn_jobp_code = $position->getJobProgressionCode();
        }

        if (isset($modifiedAttributes['budgetProfileCode'])) {
            $model->nbbposn_bpro_code = $position->getBudgetProfileCode();
        }

        if (isset($modifiedAttributes['budgetType'])) {
            $model->nbbposn_budget_type = $position->getBudgetType();
        }

        if (isset($modifiedAttributes['standardOccupationalCategoryCode'])) {
            $model->nbbposn_esoc_code = $position->getStandardOccupationalCategoryCode();
        }

        if (isset($modifiedAttributes['employmentCategoryCode'])) {
            $model->nbbposn_ecip_code = $position->getEmploymentCategoryCode();
        }

        if (isset($modifiedAttributes['comment'])) {
            $model->nbbposn_comment = $position->getComments();
        }

        $model->nbbposn_activity_date = $position->getActivityDate();
        $model->nbbposn_user_id = $position->getUserId();
        $model->nbbposn_data_origin = $position->getDataOrigin();

        $model->save();

        return $model;
    }

    /**
     * @param string $positionNumber
     * @return array
     */
    public function getPosition(string $positionNumber)
    {
        $employeePosition = new EmployeePosition();

        $positionDetails = $employeePosition::where(
            'nbbposn_posn', '=', $positionNumber
        )->get();

        $position = [];
        foreach ($positionDetails as $positionDetail) {
            $position['positionNumber'] = $positionDetail['nbbposn_posn'];
            $position['status'] = $positionDetail['nbbposn_status'];
            $position['title'] = $positionDetail['nbbposn_title'];
            $position['positionLocationCode'] = $positionDetail['nbbposn_ploc_code'];
            $position['beginDate'] = $positionDetail['nbbposn_begin_date'];
            $position['endDate'] = $positionDetail['nbbposn_end_date'];
            $position['positionType'] = $positionDetail['nbbposn_type'];
            $salarySavingAmount = PositionBudgetTotals::select('nbrptot_budget')->where('nbrptot_posn', '=', $positionDetail['nbbposn_posn'])->first();
            $position['salarySavingAmount'] = $salarySavingAmount->nbrptot_budget ?? '';
            $position['positionClassCode'] = $positionDetail['nbbposn_pcls_code'];
            $positionClassCodeDescription = PositionClassification::select('ntrpcls_desc')->where('ntrpcls_code', '=', $positionDetail['nbbposn_pcls_code'])->first();
            $position['positionClassCodeDescription'] = $positionClassCodeDescription->ntrpcls_desc ?? '';
            $position['employeeClassCode'] = $positionDetail['nbbposn_ecls_code'];
            $employeeClassCodeDescription = EmployeeClass::select('ptrecls_long_desc')->where('ptrecls_code', '=', $positionDetail['nbbposn_ecls_code'])->first();
            $position['employeeClassCodeDescription'] = $employeeClassCodeDescription->ptrecls_long_desc ?? '';
            $position['positionReportsTo' ] = $positionDetail['nbbposn_posn_reports'];
            $positionReportsToTitle = EmployeePosition::select('nbbposn_title')->where('nbbposn_posn', '=', $positionDetail['nbbposn_posn_reports'])->first();
            $position['positionReportsToTitle'] = $positionReportsToTitle->nbbposn_title ?? '';
            $position['positionAuthorizedNumber'] = $positionDetail['nbbposn_auth_number'];
            $position['salaryTable'] = $positionDetail['nbbposn_table'];
            $position['salaryGrade'] = $positionDetail['nbbposn_grade'];
            $position['salaryStep'] = $positionDetail['nbbposn_step'];
            $position['positionAppointmentPercent'] = $positionDetail['nbbposn_appt_pct'];
            $position['collegeInstructionalProgramCode'] = $positionDetail['nbbposn_cipc_code'];
            $position['budgetRollIndicator'] = $positionDetail['nbbposn_roll_ind'];
            $position['positionChartOfAccountsCode'] = $positionDetail['nbbposn_coas_code'];
            $position['activityDate'] = $positionDetail['nbbposn_activity_date'];
            $position['salaryGroup'] = $positionDetail['nbbposn_sgrp_code'];
            $position['positionGroupCode'] = $positionDetail['nbbposn_pgrp_code'];
            $positionGroupCodeDescription = PositionGroup::select('ptrpgrp_desc')->where('ptrpgrp_code', '=', $positionDetail['nbbposn_pgrp_code'])->first();
            $position['positionGroupCodeDescription'] = $positionGroupCodeDescription->ptrpgrp_desc ?? '';
            $position['workScheduleCode'] = $positionDetail['nbbposn_wksh_code'];
            $position['premiumEarningsRollIndicator'] = $positionDetail['nbbposn_premium_roll_ind'];
            $position['federalOccupationalCode'] = $positionDetail['nbbposn_pfoc_code'];
            $position['nationalOccupationalCode'] = $positionDetail['nbbposn_pnoc_code'];
            $position['occupationalTitleCode'] = $positionDetail['nbbposn_dott_code'];
            $position['changeDateTime'] = $positionDetail['nbbposn_change_date_time'];
            $position['californiaType'] = $positionDetail['nbbposn_calif_type'];
            $position['exemptionIndicator'] = $positionDetail['nbbposn_exempt_ind'];
            $position['jobLocationCode'] = $positionDetail['nbbposn_jbln_code'];
            $position['bargainingUnitCode'] = $positionDetail['nbbposn_barg_code'];
            $position['probationUnits'] = $positionDetail['nbbposn_probation_units'];
            $position['accrueSeniorityIndicator'] = $positionDetail['nbbposn_accrue_seniority_ind'];
            $position['jobProgressionCode'] = $positionDetail['nbbposn_jobp_code'];
            $position['budgetProfileCode'] = $positionDetail['nbbposn_bpro_code'];
            $position['budgetType'] = $positionDetail['nbbposn_budget_type'];
            $position['standardOccupationalCategoryCode'] = $positionDetail['nbbposn_esoc_code'];
            $standardOccupationalCategoryCodeDescription = StandardOccupationalCategory::select('ptresoc_category')->where('ptresoc_code', '=', $positionDetail['nbbposn_esoc_code'])->first();
            $position['standardOccupationalCategoryCodeDescription'] = $standardOccupationalCategoryCodeDescription->ptresoc_category ?? '';
            $position['employmentCategoryCode'] = $positionDetail['nbbposn_ecip_code'];
            $employmentCategoryCodeDescription =EmploymentCategoryCode::select('ptvecip_desc')->where('ptvecip_code', '=', $positionDetail['nbbposn_ecip_code'])->first();
            $position['employmentCategoryCodeDescription'] = $employmentCategoryCodeDescription->ptvecip_desc ?? '';
            $position['comments'] = $positionDetail['nbbposn_comment'];
            $position['userId'] = $positionDetail['nbbposn_user_id'];
            $position['dataOrigin'] = $positionDetail['nbbposn_data_origin'];
        }
        return $position;
    }

    /**
     * @param string $reportsTo
     * @param string $reportsToStatus
     * @param string $reportsToType
     * @return array
     */
    public function getReportsTo(string $reportsTo, ?string $reportsToStatus, ?string $reportsToType)
    {
        $employeePosition = new EmployeePosition();

        if(is_null($reportsToStatus) && is_null($reportsToType)) {
            $positionDetails = $employeePosition::where(
                'nbbposn_posn_reports', '=', $reportsTo
            )->get();
        }
        if(is_null($reportsToStatus) && !is_null($reportsToType)) {
            $positionDetails = $employeePosition::where(
                'nbbposn_posn_reports', '=', $reportsTo
            )
            ->where('nbbposn_type', '=', $reportsToType)
            ->get();
        }
        if(!is_null($reportsToStatus) && is_null($reportsToType)) {
            $positionDetails = $employeePosition::where(
                'nbbposn_posn_reports', '=', $reportsTo
            )
            ->where('nbbposn_status', '=', $reportsToStatus)
            ->get();
        }
        if(!is_null($reportsToStatus) && !is_null($reportsToType)) {
            $positionDetails = $employeePosition::where(
                'nbbposn_posn_reports', '=', $reportsTo
            )
            ->where('nbbposn_status', '=', $reportsToStatus)
            ->where('nbbposn_type', '=', $reportsToType)
            ->get();
        }

        $position = [];
        for($i=0; $i < count($positionDetails); $i++){
                $position[$i]['positionNumber'] = $positionDetails[$i]['nbbposn_posn'];
                $position[$i]['status'] = $positionDetails[$i]['nbbposn_status'];
                $position[$i]['title'] = $positionDetails[$i]['nbbposn_title'];
                $position[$i]['positionLocationCode'] = $positionDetails[$i]['nbbposn_ploc_code'];
                $position[$i]['beginDate'] = $positionDetails[$i]['nbbposn_begin_date'];
                $position[$i]['endDate'] = $positionDetails[$i]['nbbposn_end_date'];
                $position[$i]['positionType'] = $positionDetails[$i]['nbbposn_type'];
                $salarySavingAmount = PositionBudgetTotals::select('nbrptot_budget')->where('nbrptot_posn', '=', $positionDetails[$i]['nbbposn_posn'])->first();
                $position[$i]['salarySavingAmount'] = $salarySavingAmount->nbrptot_budget ?? '';
                $position[$i]['positionClassCode'] = $positionDetails[$i]['nbbposn_pcls_code'];
                $positionClassCodeDescription = PositionClassification::select('ntrpcls_desc')->where('ntrpcls_code', '=', $positionDetails[$i]['nbbposn_pcls_code'])->first();
                $position[$i]['positionClassCodeDescription'] = $positionClassCodeDescription->ntrpcls_desc ?? '';
                $position[$i]['employeeClassCode'] = $positionDetails[$i]['nbbposn_ecls_code'];
                $employeeClassCodeDescription = EmployeeClass::select('ptrecls_long_desc')->where('ptrecls_code', '=', $positionDetails[$i]['nbbposn_ecls_code'])->first();
                $position[$i]['employeeClassCodeDescription'] = $employeeClassCodeDescription->ptrecls_long_desc ?? '';
                $position[$i]['positionReportsTo'] = $positionDetails[$i]['nbbposn_posn_reports'];
                $positionReportsToTitle = DB::table('nbbposn')->select('nbbposn_title')->where('nbbposn_posn', '=', $positionDetails[$i]['nbbposn_posn_reports'])->first();
                $position[$i]['positionReportsToTitle'] = $positionReportsToTitle->nbbposn_title ?? '';
                $position[$i]['positionAuthorizedNumber'] = $positionDetails[$i]['nbbposn_auth_number'];
                $position[$i]['salaryTable'] = $positionDetails[$i]['nbbposn_table'];
                $position[$i]['salaryGrade'] = $positionDetails[$i]['nbbposn_grade'];
                $position[$i]['salaryStep'] = $positionDetails[$i]['nbbposn_step'];
                $position[$i]['positionAppointmentPercent'] = $positionDetails[$i]['nbbposn_appt_pct'];
                $position[$i]['collegeInstructionalProgramCode'] = $positionDetails[$i]['nbbposn_cipc_code'];
                $position[$i]['budgetRollIndicator'] = $positionDetails[$i]['nbbposn_roll_ind'];
                $position[$i]['positionChartOfAccountsCode'] = $positionDetails[$i]['nbbposn_coas_code'];
                $position[$i]['activityDate'] = $positionDetails[$i]['nbbposn_activity_date'];
                $position[$i]['salaryGroup'] = $positionDetails[$i]['nbbposn_sgrp_code'];
                $position[$i]['positionGroupCode'] = $positionDetails[$i]['nbbposn_pgrp_code'];
                $positionGroupCodeDescription = PositionGroup::select('ptrpgrp_desc')->where('ptrpgrp_code', '=', $positionDetails[$i]['nbbposn_pgrp_code'])->first();
                $position[$i]['positionGroupCodeDescription'] = $positionGroupCodeDescription->ptrpgrp_desc ?? '';
                $position[$i]['workScheduleCode'] = $positionDetails[$i]['nbbposn_wksh_code'];
                $position[$i]['premiumEarningsRollIndicator'] = $positionDetails[$i]['nbbposn_premium_roll_ind'];
                $position[$i]['federalOccupationalCode'] = $positionDetails[$i]['nbbposn_pfoc_code'];
                $position[$i]['nationalOccupationalCode'] = $positionDetails[$i]['nbbposn_pnoc_code'];
                $position[$i]['occupationalTitleCode'] = $positionDetails[$i]['nbbposn_dott_code'];
                $position[$i]['changeDateTime'] = $positionDetails[$i]['nbbposn_change_date_time'];
                $position[$i]['californiaType'] = $positionDetails[$i]['nbbposn_calif_type'];
                $position[$i]['exemptionIndicator'] = $positionDetails[$i]['nbbposn_exempt_ind'];
                $position[$i]['jobLocationCode'] = $positionDetails[$i]['nbbposn_jbln_code'];
                $position[$i]['bargainingUnitCode'] = $positionDetails[$i]['nbbposn_barg_code'];
                $position[$i]['probationUnits'] = $positionDetails[$i]['nbbposn_probation_units'];
                $position[$i]['accrueSeniorityIndicator'] = $positionDetails[$i]['nbbposn_accrue_seniority_ind'];
                $position[$i]['jobProgressionCode'] = $positionDetails[$i]['nbbposn_jobp_code'];
                $position[$i]['budgetProfileCode'] = $positionDetails[$i]['nbbposn_bpro_code'];
                $position[$i]['budgetType'] = $positionDetails[$i]['nbbposn_budget_type'];
                $position[$i]['standardOccupationalCategoryCode'] = $positionDetails[$i]['nbbposn_esoc_code'];
                $standardOccupationalCategoryCodeDescription = DB::table('ptresoc')->select('ptresoc_category')->where('ptresoc_code', '=', $positionDetails[$i]['nbbposn_esoc_code'])->first();
                $position[$i]['standardOccupationalCategoryCodeDescription'] = $standardOccupationalCategoryCodeDescription->ptresoc_category ?? '';
                $position[$i]['employmentCategoryCode'] = $positionDetails[$i]['nbbposn_ecip_code'];
                $employmentCategoryCodeDescription = DB::table('ptvecip')->select('ptvecip_desc')->where('ptvecip_code', '=', $positionDetails[$i]['nbbposn_ecip_code'])->first();
                $position[$i]['employmentCategoryCodeDescription'] = $employmentCategoryCodeDescription->ptvecip_desc ?? '';
                $position[$i]['comments'] = $positionDetails[$i]['nbbposn_comment'];
                $position[$i]['userId'] = $positionDetails[$i]['nbbposn_user_id'];
                $position[$i]['dataOrigin'] = $positionDetails[$i]['nbbposn_data_origin'];
        }

        return $position;
    }

}
