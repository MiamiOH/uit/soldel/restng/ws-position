<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 11/27/18
 * Time: 1:36 PM
 */

namespace MiamiOH\PositionWebService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

//use MiamiOH\PositionWebService\Services\EmployeePosition;

class EmployeePositionResourceProvider extends ResourceProvider
{
    private $application = 'WebServices';
    private $module = 'Employee';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => 'EmployeePosition.Get.Model',
            'type' => 'object',
            'properties' => [
                'position' => [
                    'type' => 'string',
                ],
                'status' => [
                    'type' => 'string',
                ],
                'title' => [
                    'type' => 'string',
                ],
                'positionLocationCode' => [
                    'type' => 'string',
                ],
                'beginDate' => [
                    'type' => 'string',
                ],
                'endDate' => [
                    'type' => 'string',
                ],
                'positionType' => [
                    'type' => 'string',
                ],
                'salarySavingAmount' => [
                    'type' => 'string',
                ],
                'positionClassCode' => [
                    'type' => 'string',
                ],
                'positionClassCodeDescription' => [
                    'type' => 'string',
                ],
                'employeeClassCode' => [
                    'type' => 'string',
                ],
                'employeeClassCodeDescription' => [
                    'type' => 'string',
                ],
                'positionReportsTo' => [
                    'type' => 'string',
                ],
                'positionReportsToTitle' => [
                    'type' => 'string',
                ],
                'positionAuthorizedNumber' => [
                    'type' => 'string',
                ],
                'salaryTable' => [
                    'type' => 'string',
                ],
                'salaryGrade' => [
                    'type' => 'string',
                ],
                'salaryStep' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'positionAppointmentPercent' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'collegeInstructionalProgramCode' => [
                    'type' => 'string',
                ],
                'budgetRollIndicator' => [
                    'type' => 'string',
                ],
                'positionChartOfAccountsCode' => [
                    'type' => 'string',
                ],
                'activityDate' => [
                    'type' => 'string',
                ],
                'salaryGroup' => [
                    'type' => 'string',
                ],
                'positionGroupCode' => [
                    'type' => 'string',
                ],
                'positionGroupCodeDescription' => [
                    'type' => 'string',
                ],
                'workScheduleCode' => [
                    'type' => 'string',
                ],
                'premiumEarningsRollIndicator' => [
                    'type' => 'string',
                ],
                'federalOccupationalCode' => [
                    'type' => 'string',
                ],
                'nationalOccupationalCode' => [
                    'type' => 'string',
                ],
                'occupationalTitleCode' => [
                    'type' => 'string',
                ],
                'changeDateTime' => [
                    'type' => 'string',
                ],
                'californiaType' => [
                    'type' => 'string',
                ],
                'exemptionIndicator' => [
                    'type' => 'string',
                ],
                'jobLocationCode' => [
                    'type' => 'string',
                ],
                'bargainingUnitCode' => [
                    'type' => 'string',
                ],
                'probationUnits' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'accrueSeniorityIndicator' => [
                    'type' => 'string',
                ],
                'jobProgressionCode' => [
                    'type' => 'string',
                ],
                'budgetProfileCode' => [
                    'type' => 'string',
                ],
                'budgetType' => [
                    'type' => 'string',
                ],
                'standardOccupationalCategoryCode' => [
                    'type' => 'string',
                ],
                'standardOccupationalCategoryCodeDescription' => [
                    'type' => 'string',
                ],
                'employmentCategoryCode' => [
                    'type' => 'string',
                ],
                'employmentCategoryCodeDescription' => [
                    'type' => 'string',
                ],
                'comments' => [
                    'type' => 'string',
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'EmployeePosition.Post.Model',
            'type' => 'object',
            'properties' => [
                'position' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'status' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'title' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'positionLocationCode' => [
                    'type' => 'string',
                ],
                'beginDate' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'endDate' => [
                    'type' => 'string',
                ],
                'positionType' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],

                ],
                'positionClassCode' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'employeeClassCode' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'positionReportsTo' => [
                    'type' => 'string',
                ],
                'positionAuthorizedNumber' => [
                    'type' => 'string',
                ],
                'salaryTable' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'salaryGrade' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'salaryStep' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'positionAppointmentPercent' => [
                    'type' => 'number',
                    'format' => 'int32',
                    'enum'=> ['required|number'],
                ],
                'collegeInstructionalProgramCode' => [
                    'type' => 'string',
                ],
                'budgetRollIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'positionChartOfAccountsCode' => [
                    'type' => 'string',
                ],
                'salaryGroup' => [
                    'type' => 'string',
                ],
                'positionGroupCode' => [
                    'type' => 'string',
                ],
                'workScheduleCode' => [
                    'type' => 'string',
                ],
                'premiumEarningsRollIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'federalOccupationalCode' => [
                    'type' => 'string',
                ],
                'nationalOccupationalCode' => [
                    'type' => 'string',
                ],
                'occupationalTitleCode' => [
                    'type' => 'string',
                ],
                'changeDateTime' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'californiaType' => [
                    'type' => 'string',
                ],
                'exemptionIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'jobLocationCode' => [
                    'type' => 'string',
                ],
                'bargainingUnitCode' => [
                    'type' => 'string',
                ],
                'probationUnits' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'accrueSeniorityIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'jobProgressionCode' => [
                    'type' => 'string',
                ],
                'budgetProfileCode' => [
                    'type' => 'string',
                ],
                'budgetType' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'standardOccupationalCategoryCode' => [
                    'type' => 'string',
                ],
                'employmentCategoryCode' => [
                    'type' => 'string',
                ],
                'comments' => [
                    'type' => 'string',
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => 'EmployeePosition.Put.Model',
            'type' => 'object',
            'properties' => [
                'status' => [
                    'type' => 'string',
                ],
                'title' => [
                    'type' => 'string',
                ],
                'positionLocationCode' => [
                    'type' => 'string',
                ],
                'beginDate' => [
                    'type' => 'string',
                ],
                'endDate' => [
                    'type' => 'string',
                ],
                'positionType' => [
                    'type' => 'string',
                ],
                'positionClassCode' => [
                    'type' => 'string',
                ],
                'employeeClassCode' => [
                    'type' => 'string',
                ],
                'positionReportsTo' => [
                    'type' => 'string',
                ],
                'positionAuthorizedNumber' => [
                    'type' => 'string',
                ],
                'salaryTable' => [
                    'type' => 'string',
                ],
                'salaryGrade' => [
                    'type' => 'string',
                ],
                'salaryStep' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'positionAppointmentPercent' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'collegeInstructionalProgramCode' => [
                    'type' => 'string',
                ],
                'budgetRollIndicator' => [
                    'type' => 'string',
                ],
                'positionChartOfAccountsCode' => [
                    'type' => 'string',
                ],
                'salaryGroup' => [
                    'type' => 'string',
                ],
                'positionGroupCode' => [
                    'type' => 'string',
                ],
                'workScheduleCode' => [
                    'type' => 'string',
                ],
                'premiumEarningsRollIndicator' => [
                    'type' => 'string',
                ],
                'federalOccupationalCode' => [
                    'type' => 'string',
                ],
                'nationalOccupationalCode' => [
                    'type' => 'string',
                ],
                'occupationalTitleCode' => [
                    'type' => 'string',
                ],
                'changeDateTime' => [
                    'type' => 'string',
                ],
                'californiaType' => [
                    'type' => 'string',
                ],
                'exemptionIndicator' => [
                    'type' => 'string',
                ],
                'jobLocationCode' => [
                    'type' => 'string',
                ],
                'bargainingUnitCode' => [
                    'type' => 'string',
                ],
                'probationUnits' => [
                    'type' => 'number',
                    'format' => 'int32',
                ],
                'accrueSeniorityIndicator' => [
                    'type' => 'string',
                ],
                'jobProgressionCode' => [
                    'type' => 'string',
                ],
                'budgetProfileCode' => [
                    'type' => 'string',
                ],
                'budgetType' => [
                    'type' => 'string',
                ],
                'standardOccupationalCategoryCode' => [
                    'type' => 'string',
                ],
                'employmentCategoryCode' => [
                    'type' => 'string',
                ],
                'comments' => [
                    'type' => 'string',
                ]
            ]
        ]);

        $this->addDefinition([
                'name' => 'EmployeePosition.Get.Collection',
                'type' => 'array',
                'items' => [
                    '$ref' => '#/definitions/EmployeePosition.Get.Model'
                ]
            ]
        );

        $this->addDefinition([
            'name' => 'EmployeePosition.Post.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/EmployeePosition.Post.Model'
            ]
        ]);

        $this->addDefinition([
            'name' => 'EmployeePosition.Put.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/EmployeePosition.Put.Model'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => 'EmployeePosition',
            'class' => 'MiamiOH\PositionWebService\Services\EmployeePosition',
            'description' => 'Resource for retrieving and inserting employee\'s position details',
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'name' => 'EmployeePosition.v1.get',
            'description' => 'Retrieves position details of employees',
            'pattern' => '/position/v1',
            'service' => 'EmployeePosition',
            'tags' => ['position'],
            'method' => 'getPositionDetails',
            'isPageable' => true,
            'isPartialable' => true,
            'defaultPageLimit' => 50,
            'maxPageLimit' => 500,
            'options' => [
                'positionNumber' => [
                    'type' => 'list',
                    'description' => 'Return the position that matches position number'
                ],
                'reportsTo' => [
                    'type' => 'list',
                    'description' => 'Return the position(s) that report to position number'
                ],
                'reportsToStatus' => [
                    'description' => 'Specify position status for ReportsTo request - A, C, I, or F. Default: all statuses'
                ],
                'reportsToType' => [
                    'description' => 'Specify position type for ReportsTo request - S for Single, P for Pooled. Default: all types'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of position records.',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/EmployeePosition.Get.Collection',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'name' => 'EmployeePosition.v1.create',
            'description' => 'Creates multiple positions for employees',
            'pattern' => '/position/v1',
            'tags' => ['position'],
            'service' => 'EmployeePosition',
            'method' => 'createPositionDetails',
            'returnType' => 'model',
            'body' => [
                'description' => 'Collection of Employee Position objects',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/EmployeePosition.Post.Model',
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],

            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'name' => 'EmployeePosition.v1.update',
            'description' => 'Update position of an employee',
            'pattern' => '/position/v1/:positionNumber',
            'service' => 'EmployeePosition',
            'tags' => ['position'],
            'method' => 'updatePositionDetails',
            'returnType' => 'model',
            'body' => [
                'description' => 'Collection of Employee Position objects',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/EmployeePosition.Put.Model',
                ]
            ],
            'params' => array(
                'positionNumber' => array(
                    'type' => 'single',
                    'description' => 'Position Number'
                ),
            ),
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],

            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {

    }
}
