<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 11/27/18
 * Time: 1:38 PM
 */

namespace MiamiOH\PositionWebService\Services;

use MiamiOH\PositionWebService\Repositories\PositionSQL;
use MiamiOH\PositionWebService\Utilities\RESTngEloquentFactory;
use MiamiOH\PositionWebService\Utilities\RESTngValidatorFactory;
use MiamiOH\RESTng\Legacy\DataSource;
use function print_r;

class EmployeePosition extends \MiamiOH\RESTng\Service
{
    private $rules = [
        'position' => 'required|max:6',
        'status' => 'required|max:1',
        'title' => 'required|max:30',
        'positionLocationCode' => 'max:6',
        'beginDate' => 'required',
        'positionType' => 'required|max:1',
        'positionClassCode' => 'required|max:5',
        'employeeClassCode' => 'required|max:2',
        'salaryTable' => 'required|max:2',
        'salaryGrade' => 'required|max:5',
        'positionAppointmentPercent' => 'required',
        'budgetRollIndicator' => 'required|max:1',
        'changeDateTime' => 'required',
        'exemptionIndicator' => 'required|max:1',
        'accrueSeniorityIndicator' => 'required|max:1',
        'budgetType' => 'required|max:1',
    ];

    private $messages = [
        'unique' => 'The :attribute has already been taken.',
        'required' => 'The :attribute field is required.',
        'max' => 'Maximum length exceeded for :attribute.',
    ];

    public function getPositionDetails()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        $positionSql = new PositionSQL(new \MiamiOH\PositionWebService\EloquentModels\EmployeePosition());

        if(empty($options['reportsTo'][0])){

            if (empty($options['positionNumber'][0])) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Please provide position number.');
            }

            $positionDetails = $positionSql->getPosition($options['positionNumber'][0]);
        } else {
            $reportsToStatus = isset($options['reportsToStatus']) ? $options['reportsToStatus'] : null;
            $reportsToType = isset($options['reportsToType']) ? $options['reportsToType'] : null;

            $positionDetails = $positionSql->getReportsTo($options['reportsTo'][0], $reportsToStatus, $reportsToType);
        }

        if (empty($positionDetails)) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('No record found.');
        }

        $response->setStatus('API_OK');
        $response->setPayload($positionDetails);

        return $response;
    }

    private $updateRules = [
        'status' => 'max:1',
        'title' => 'max:30',
        'positionLocationCode' => 'max:6',
        'beginDate' => 'date',
        'positionType' => 'max:1',
        'positionClassCode' => 'max:5',
        'employeeClassCode' => 'max:2',
        'salaryTable' => 'max:2',
        'salaryGrade' => 'max:5',
        'positionAppointmentPercent' => '',
        'budgetRollIndicator' => 'max:1',
        'changeDateTime' => 'date',
        'exemptionIndicator' => 'max:1',
        'accrueSeniorityIndicator' => 'max:1',
        'budgetType' => 'max:1',
    ];

    public function updatePositionDetails()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $positionNumber = $request->getResourceParam('positionNumber');

        $user = $this->getApiUser();

        $data = $request->getData();

        # Make sure data is not empty
        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }

        $validator = RESTngValidatorFactory::make($request->getData(), $this->updateRules, $this->messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $payLoad['errors'][] = $errors->getMessageBag();
            $response->setPayLoad($payLoad);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WebService';
        $data['position'] = $positionNumber;

        $positionObject = Position::fromArray($data);

        $positionSql = new PositionSQL();
        $model = $positionSql->updatePosition($positionObject);

        $response->setPayload(['1 record updated.']);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);


        return $response;
    }


    public function createPositionDetails()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $data = $request->getData();

        # Make sure data is not empty
        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }

        $validator = RESTngValidatorFactory::make($request->getData(), $this->rules, $this->messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $payLoad['errors'][] = $errors->getMessageBag();
            $response->setPayLoad($payLoad);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WebService';

        $positionObject = Position::fromArray($data);

        $positionSql = new PositionSQL();
        $position = $positionSql->createPosition($positionObject);

        $response->setPayload(['1 record created.']);
        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);

        return $response;
    }
}
