<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 12/1/18
 * Time: 9:58 PM
 */

namespace MiamiOH\PositionWebService\Services;

use Exception;

/**
 * Class EmployeePositionDA
 * @package MiamiOH\WsPosition\DataAccesses
 */
class Position
{
    /**
     *
     */
    const ORACLE_DATE_FORMAT = 'Y-m-d H:i:s';


    /**
     * @var string
     */
    private $position = '';

    /**
     * @var string
     */
    private $status = '';

    /**
     * @var string
     */

    private $title = '';

    /**
     * @var string
     */
    private $positionLocationCode = '';

    /**
     * @var \DateTime
     */
    private $beginDate = null;

    /**
     * @var \DateTime
     */
    private $endDate = null;

    /**
     * @var string
     */
    private $positionType = '';

    /**
     * @var string
     */
    private $positionClassCode = '';

    /**
     * @var string
     */
    private $employeeClassCode = '';

    /**
     * @var string
     */
    private $positionReportsTo = '';

    /**
     * @var string
     */
    private $positionAuthorizedNumber = '';

    /**
     * @var string
     */
    private $salaryTable = '';

    /**
     * @var string
     */
    private $salaryGrade = '';

    /**
     * @var string
     */
    private $salaryStep = '';

    /**
     * @var string
     */
    private $positionAppointmentPercent = '';

    /**
     * @var string
     */
    private $collegeInstructionalProgramCode = '';

    /**
     * @var string
     */
    private $budgetRollIndicator = '';

    /**
     * @var string
     */
    private $positionChartOfAccountsCode = '';

    /**
     * @var string
     */
    private $activityDate = '';

    /**
     * @var string
     */
    private $salaryGroup = '';

    /**
     * @var string
     */
    private $positionGroupCode = '';

    /**
     * @var string
     */
    private $workScheduleCode = '';

    /**
     * @var string
     */
    private $premiumEarningsRollIndicator = '';

    /**
     * @var string
     */
    private $federalOccupationalCode = '';

    /**
     * @var string
     */
    private $nationalOccupationalCode = '';

    /**
     * @var string
     */
    private $occupationalTitleCode = '';

    /**
     * @var string
     */
    private $changeDateTime = '';

    /**
     * @var string
     */
    private $californiaType = '';

    /**
     * @var string
     */
    private $exemptionIndicator = '';

    /**
     * @var string
     */
    private $jobLocationCode = '';

    /**
     * @var string
     */
    private $bargainingUnitCode = '';

    /**
     * @var string
     */
    private $probationUnits = '';

    /**
     * @var string
     */
    private $accrueSeniorityIndicator = '';

    /**
     * @var string
     */
    private $jobProgressionCode = '';

    /**
     * @var string
     */
    private $budgetProfileCode = '';

    /**
     * @var string
     */
    private $budgetType = '';

    /**
     * @var string
     */
    private $standardOccupationalCategoryCode = '';

    /**
     * @var string
     */
    private $employmentCategoryCode = '';

    /**
     * @var string
     */
    private $userId = '';

    /**
     * @var string
     */
    private $dataOrigin = '';

    /**
     * @var string
     */
    private $comments = '';


    /**
     * @param $position
     * @return Position
     */
    public function setPosition(string $position): Position
    {
        $this->position = $position;
        return $this;
    }


    /**
     * @param $status
     * @return Position
     */
    public function setStatus(string $status): Position
    {
        $this->status = $status;
        return $this;
    }


    /**
     * @param $title
     * @return Position
     */
    public function setTitle(string $title): Position
    {
        $this->title = $title;
        return $this;
    }


    /**
     * @param $positionLocationCode
     * @return Position
     */
    public function setPositionLocationCode(string $positionLocationCode): Position
    {
        $this->positionLocationCode = $positionLocationCode;
        return $this;
    }


    /**
     * @param string $beginDate
     * @return Position
     * @throws Exception
     */
    public function setBeginDate(string $beginDate): Position
    {
        if (empty($beginDate)) {
            $this->beginDate = null;
        } else {
            $this->beginDate = new \DateTime($beginDate);
        }
        return $this;
    }


    /**
     * @param $endDate
     * @return Position
     * @throws Exception
     */
    public function setEndDate(string $endDate): Position
    {
        if (empty($endDate)) {
            $this->endDate = null;
        } else {
            $this->endDate = new \DateTime($endDate);
        }

        return $this;

    }


    /**
     * @param $positionType
     * @return Position
     */
    public function setPositionType(string $positionType): Position
    {
        $this->positionType = $positionType;
        return $this;
    }


    /**
     * @param $positionClassCode
     * @return Position
     */
    public function setPositionClassCode(string $positionClassCode): Position
    {
        $this->positionClassCode = $positionClassCode;
        return $this;
    }


    /**
     * @param $employeeClassCode
     * @return Position
     */
    public function setEmployeeClassCode(string $employeeClassCode): Position
    {
        $this->employeeClassCode = $employeeClassCode;
        return $this;
    }


    /**
     * @param $positionReportsTo
     * @return Position
     */
    public function setPositionReportsTo(string $positionReportsTo): Position
    {
        $this->positionReportsTo = $positionReportsTo;
        return $this;
    }


    /**
     * @param $positionAuthorizedNumber
     * @return Position
     */
    public function setPositionAuthorizedNumber(string $positionAuthorizedNumber): Position
    {
        $this->positionAuthorizedNumber = $positionAuthorizedNumber;
        return $this;
    }


    /**
     * @param $salaryTable
     * @return Position
     */
    public function setSalaryTable(string $salaryTable): Position
    {
        $this->salaryTable = $salaryTable;
        return $this;
    }


    /**
     * @param $salaryGrade
     * @return Position
     */
    public function setSalaryGrade(string $salaryGrade): Position
    {
        $this->salaryGrade = $salaryGrade;
        return $this;
    }


    /**
     * @param $salaryStep
     * @return Position
     */
    public function setSalaryStep(string $salaryStep): Position
    {
        $this->salaryStep = $salaryStep;
        return $this;
    }


    /**
     * @param $positionAppointmentPercent
     * @return Position
     */
    public function setPositionAppointmentPercent(string $positionAppointmentPercent): Position
    {
        $this->positionAppointmentPercent = $positionAppointmentPercent;
        return $this;
    }


    /**
     * @param $collegeInstructionalProgramCode
     * @return Position
     */
    public function setCollegeInstructionalProgramCode(string $collegeInstructionalProgramCode): Position
    {
        $this->collegeInstructionalProgramCode = $collegeInstructionalProgramCode;
        return $this;
    }


    /**
     * @param $budgetRollIndicator
     * @return Position
     */
    public function setBudgetRollIndicator(string $budgetRollIndicator): Position
    {
        $this->budgetRollIndicator = $budgetRollIndicator;
        return $this;
    }


    /**
     * @param $positionChartOfAccountsCode
     * @return Position
     */
    public function setPositionChartOfAccountsCode(string $positionChartOfAccountsCode): Position
    {
        $this->positionChartOfAccountsCode = $positionChartOfAccountsCode;
        return $this;
    }

    /**
     * @param $salaryGroup
     * @return Position
     */
    public function setSalaryGroup(string $salaryGroup): Position
    {
        $this->salaryGroup = $salaryGroup;
        return $this;
    }


    /**
     * @param $positionGroupCode
     * @return Position
     */
    public function setPositionGroupCode(string $positionGroupCode): Position
    {
        $this->positionGroupCode = $positionGroupCode;
        return $this;
    }


    /**
     * @param $workScheduleCode
     * @return Position
     */
    public function setWorkScheduleCode(string $workScheduleCode): Position
    {
        $this->workScheduleCode = $workScheduleCode;
        return $this;
    }


    /**
     * @param $premiumEarningsRollIndicator
     * @return Position
     */
    public function setPremiumEarningsRollIndicator(string $premiumEarningsRollIndicator): Position
    {
        $this->premiumEarningsRollIndicator = $premiumEarningsRollIndicator;
        return $this;
    }


    /**
     * @param $federalOccupationalCode
     * @return Position
     */
    public function setFederalOccupationalCode(string $federalOccupationalCode): Position
    {
        $this->federalOccupationalCode = $federalOccupationalCode;
        return $this;
    }


    /**
     * @param $nationalOccupationalCode
     * @return Position
     */
    public function setNationalOccupationalCode(string $nationalOccupationalCode): Position
    {
        $this->nationalOccupationalCode = $nationalOccupationalCode;
        return $this;
    }


    /**
     * @param $occupationalTitleCode
     * @return Position
     */
    public function setOccupationalTitleCode(string $occupationalTitleCode): Position
    {
        $this->occupationalTitleCode = $occupationalTitleCode;
        return $this;
    }


    /**
     * @param $changeDateTime
     * @return Position
     * @throws Exception
     */
    public function setChangeDateTime(string $changeDateTime): Position
    {
        if (empty($changeDateTime)) {
            $this->changeDateTime = null;
        } else {
            $this->changeDateTime = new \DateTime($changeDateTime);
        }

        return $this;

    }

    /**
     * @param $californiaType
     * @return Position
     */
    public function setCaliforniaType(string $californiaType): Position
    {
        $this->californiaType = $californiaType;
        return $this;
    }

    /**
     * @param $exemptionIndicator
     * @return Position
     */
    public function setExemptionIndicator(string $exemptionIndicator): Position
    {
        $this->exemptionIndicator = $exemptionIndicator;
        return $this;
    }

    /**
     * @param $jobLocationCode
     * @return Position
     */
    public function setJobLocationCode(string $jobLocationCode): Position
    {
        $this->jobLocationCode = $jobLocationCode;
        return $this;
    }


    /**
     * @param $bargainingUnitCode
     * @return Position
     */
    public function setBargainingUnitCode(string $bargainingUnitCode): Position
    {
        $this->bargainingUnitCode = $bargainingUnitCode;
        return $this;
    }


    /**
     * @param $probationUnits
     * @return Position
     */
    public function setProbationUnits(string $probationUnits): Position
    {
        $this->probationUnits = $probationUnits;
        return $this;
    }


    /**
     * @param $accrueSeniorityIndicator
     * @return Position
     */
    public function setAccrueSeniorityIndicator(string $accrueSeniorityIndicator): Position
    {
        $this->accrueSeniorityIndicator = $accrueSeniorityIndicator;
        return $this;
    }


    /**
     * @param $jobProgressionCode
     * @return Position
     */
    public function setJobProgressionCode(string $jobProgressionCode): Position
    {
        $this->jobProgressionCode = $jobProgressionCode;
        return $this;
    }


    /**
     * @param $budgetProfileCode
     * @return Position
     */
    public function setBudgetProfileCode(string $budgetProfileCode): Position
    {
        $this->budgetProfileCode = $budgetProfileCode;
        return $this;
    }


    /**
     * @param $budgetType
     * @return Position
     */
    public function setBudgetType(string $budgetType): Position
    {
        $this->budgetType = $budgetType;
        return $this;
    }


    /**
     * @param $standardOccupationalCategoryCode
     * @return Position
     */
    public function setStandardOccupationalCategoryCode(string $standardOccupationalCategoryCode): Position
    {
        $this->standardOccupationalCategoryCode = $standardOccupationalCategoryCode;
        return $this;
    }


    /**
     * @param $employmentCategoryCode
     * @return Position
     */
    public function setEmploymentCategoryCode(string $employmentCategoryCode): Position
    {
        $this->employmentCategoryCode = $employmentCategoryCode;
        return $this;
    }


    /**
     * @param $userId
     * @return Position
     */
    public function setUserId(string $userId): Position
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param $dataOrigin
     * @return Position
     */
    public function setDataOrigin(string $dataOrigin): Position
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }

    /**
     * @param $comments
     * @return Position
     */
    public function setComments(string $comments): Position
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getPositionLocationCode(): string
    {
        return $this->positionLocationCode;
    }

    /**
     * @return string
     */
    public function getBeginDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->beginDate)) {
            return '';
        }
        return $this->beginDate->format($format);

    }


    /**
     * @return string
     */
    public function getEndDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->endDate)) {
            return '';
        }

        return $this->endDate->format($format);
    }


    /**
     * @return string
     */
    public function getPositionType(): string
    {
        return $this->positionType;
    }


    /**
     * @return string
     */
    public function getPositionClassCode(): string
    {
        return $this->positionClassCode;
    }


    /**
     * @return string
     */
    public function getEmployeeClassCode(): string
    {
        return $this->employeeClassCode;
    }


    /**
     * @return string
     */
    public function getPositionReportsTo(): string
    {
        return $this->positionReportsTo;
    }


    /**
     * @return string
     */
    public function getPositionAuthorizedNumber(): string
    {
        return $this->positionAuthorizedNumber;
    }


    /**
     * @return string
     */
    public function getSalaryTable(): string
    {
        return $this->salaryTable;
    }


    /**
     * @return string
     */
    public function getSalaryGrade(): string
    {
        return $this->salaryGrade;
    }


    /**
     * @return string
     */
    public function getSalaryStep(): string
    {
        return $this->salaryStep;
    }


    /**
     * @return string
     */
    public function getPositionAppointmentPercent(): string
    {
        return $this->positionAppointmentPercent;
    }


    /**
     * @return string
     */
    public function getCollegeInstructionalProgramCode(): string
    {
        return $this->collegeInstructionalProgramCode;
    }


    /**
     * @return string
     */
    public function getBudgetRollIndicator(): string
    {
        return $this->budgetRollIndicator;
    }


    /**
     * @return string
     */
    public function getPositionChartOfAccountsCode(): string
    {
        return $this->positionChartOfAccountsCode;
    }

    /**
     * @return \DateTime
     * @throws Exception
     */
    public function getActivityDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        return (new \DateTime('now'))->format($format);
    }

    /**
     * @return string
     */
    public function getSalaryGroup(): string
    {
        return $this->salaryGroup;
    }

    /**
     * @return string
     */
    public function getPositionGroupCode(): string
    {
        return $this->positionGroupCode;
    }

    /**
     * @return string
     */
    public function getWorkScheduleCode(): string
    {
        return $this->workScheduleCode;
    }

    /**
     * @return string
     */
    public function getPremiumEarningsRollIndicator(): string
    {
        return $this->premiumEarningsRollIndicator;
    }

    /**
     * @return mixed
     */
    public function getFederalOccupationalCode(): string
    {
        return $this->federalOccupationalCode;
    }

    /**
     * @return mixed
     */
    public function getNationalOccupationalCode(): string
    {
        return $this->nationalOccupationalCode;
    }

    /**
     * @return mixed
     */
    public function getOccupationalTitleCode(): string
    {
        return $this->occupationalTitleCode;
    }

    /**
     * @return string
     */
    public function getChangeDateTime(string $format = self::ORACLE_DATE_FORMAT): string
    {
        return (new \DateTime('now'))->format($format);
    }

    /**
     * @return string
     */
    public function getCaliforniaType(): string
    {
        return $this->californiaType;
    }

    /**
     * @return string
     */
    public function getExemptionIndicator(): string
    {
        return $this->exemptionIndicator;
    }

    /**
     * @return string
     */
    public function getJobLocationCode(): string
    {
        return $this->jobLocationCode;
    }

    /**
     * @return string
     */
    public function getBargainingUnitCode(): string
    {
        return $this->bargainingUnitCode;
    }

    /**
     * @return string
     */
    public function getProbationUnits(): string
    {
        return $this->probationUnits;
    }

    /**
     * @return string
     */
    public function getAccrueSeniorityIndicator(): string
    {
        return $this->accrueSeniorityIndicator;
    }

    /**
     * @return string
     */
    public function getJobProgressionCode(): string
    {
        return $this->jobProgressionCode;
    }

    /**
     * @return string
     */
    public function getBudgetProfileCode(): string
    {
        return $this->budgetProfileCode;
    }

    /**
     * @return string
     */
    public function getBudgetType(): string
    {
        return $this->budgetType;
    }

    /**
     * @return string
     */
    public function getStandardOccupationalCategoryCode(): string
    {
        return $this->standardOccupationalCategoryCode;
    }

    /**
     * @return string
     */
    public function getEmploymentCategoryCode(): string
    {
        return $this->employmentCategoryCode;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin = 'webService';
    }

    /**
     * @return string
     */
    public function getComments(): string
    {
        return $this->comments;
    }

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    /**
     * @param array $positionData
     * @return Position
     * @throws Exception
     */
    public static function fromArray(array $positionData): self
    {
        $thisInstance = new self();

        try {
            foreach ($positionData as $key => $val) {
                $thisInstance->{'set' . ucfirst($key)}($val);
                $thisInstance->addModifiedAttributes($key);
            }
        } catch (Exception $e) {
            throw new Exception("Invalid key for Position: $key" . $e->getMessage());
        }

        return $thisInstance;
    }
}