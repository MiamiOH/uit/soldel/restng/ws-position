# ws-position
Web Service for obtaining Employee Position information

## Local Development

### Prerequisites
In order to start local development, please make sure following prerequisites are met:
- Docker installed
- Docker Compose

### Setup
- Clone the repo.  The "Run" commands below should be run from the project's directory.
- Copy docker/config/authorizations.yaml.example to docker/config/authorizations.yaml.  Update the values if needed.
- Copy docker/config/credentials.yaml.example to docker/config/credentials.yaml.  Update the passwords and tokens to your liking.
- Copy docker/config/datasources.yaml.example to docker/config/datasources.yaml.  Update the passwords with the actual passwords to connect to Oracle development instances.  If you do not know them, see a application developer or DBA.
- Run docker-compose up -d to start the containers.
- Run docker run -it --rm -v $(pwd):/opt/project -v ~/.ssh:/root/.ssh miamioh/php:7.3-phpstorm composer install to install composer dependencies in the main project.  You may be prompted for your SSH id_rsa passhrase.
- Access https://localhost/swagger-ui/ to confirm the containers and application are working.  The resources for this project should be listed.

### Parameters
- **positionNumber**: Return the position that matches position number
- **reportsTo:** Return the position(s) that report to position number
- **reportsToStatus:** Specify position status for ReportsTo request - A, C, I, or F. Default: all statuses
- **reportsToType:** Specify position type for ReportsTo request - S for Single, P for Pooled. Default: all types
- **limit:** The number of objects to return Default Page Limit: 50 Maximum Page Limit: 500
- **offset:** The number of the first object to return
- **fields:** List of object fields and/or subobjects to include

#### Notes on Parameters
If reportsTo is specified, positionNumber will be ignored. If reportsTo is not specified, but positionNumber is, 
reportsToType and reportsToStatus will be ignored.

reportsTo returns all positions whose nbbposn_posn_reports equals the specified position number in the reportsTo field

## Authorization
Authorization is done through CAM
- Application Name: WebServices
- Category Name: Employee
- Key Name: all
